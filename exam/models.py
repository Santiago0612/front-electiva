from django.db import models
from django.core.validators import RegexValidator
# Create your models here.

class CoffeModel(models.Model):
        
    texto_validator = RegexValidator(
        regex=r'^[a-zA-ZáéíóúÁÉÍÓÚñÑüÜ\s]+$',
        message='Este campo solo debe contener letras.'
    )
    nombre=models.CharField(max_length=20, null=False, blank=False, validators=[texto_validator])
    tipo=models.CharField(max_length=20,null=False, blank=False, validators=[texto_validator])
    region=models.CharField(max_length=20, null=False, blank=False, validators=[texto_validator])
