from django.urls import path
from .views import CoffeApiView
urlpatterns = [
    path('cafe', CoffeApiView.as_view(), name = 'listar_cafe'),
]