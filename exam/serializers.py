from rest_framework import serializers
from .models import CoffeModel

class CoffeSerializer(serializers.ModelSerializer):
    class Meta:
        model=CoffeModel
        fields=['id', 'nombre', 'tipo', 'region']