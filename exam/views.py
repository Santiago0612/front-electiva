from django.shortcuts import render
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from .serializers import CoffeSerializer
from .models import CoffeModel

# Create your views here.
class CoffeApiView(APIView):
     

     def get(self, request):
        coffees = CoffeModel.objects.all()
        serializer = CoffeSerializer(coffees, many=True)
       
        return Response(serializer.data, status=status.HTTP_200_OK)
     
     def post(self, request):
         serializer = CoffeSerializer(data=request.data)
         if serializer.is_valid():
             serializer.save()
             return Response(serializer.data, status=status.HTTP_201_CREATED)
         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)